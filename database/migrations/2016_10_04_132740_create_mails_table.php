<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mails', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('sender_id')->unsigned()->index()->references('id')->on('users')->onDelete('cascade');
            $table->integer('recipient_id')->unsigned()->index()->references('id')->on('users')->onDelete('cascade');
            $table->String("header")->nullable();
            $table->text("body")->nullable();
            $table->date("sent_at")->nullable();
            $table->timestamps();
        });

        Schema::create('mail_metadata', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('mail_id')->index()->references('id')->on('mails')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index()->references('id')->on('users')->onDelete('cascade');
            $table->integer("status_id")->default(1)->references('id')->on('mail_statuses');
            $table->boolean("is_favorite")->default(false);
            $table->boolean("is_read")->default(false);
            $table->date("read_at")->nullable();
            $table->timestamps();
        });

        Schema::create('mail_statuses', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->String('name');
        });

        $statuses = [
            ['id' => 1, 'name' => 'regular'],
            ['id' => 2, 'name' => 'trash'],
            ['id' => 3, 'name' => 'spam'],
            ['id' => 4, 'name' => 'draft'],
        ];

        \App\Models\MailStatus::insert($statuses);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mails');
        Schema::dropIfExists('mail_metadata');
        Schema::dropIfExists('mail_statuses');
    }
}
