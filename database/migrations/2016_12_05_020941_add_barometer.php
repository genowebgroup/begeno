<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBarometer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table){
            $table->date("available")->nullable()->after('email');
            $table->integer("barometer")->default(0)->after('email');
        });

        Schema::table('users', function (Blueprint $table){
            $table->integer("login_count")->default(0)->after('role_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function ($table) {
            $table->dropColumn('barometer');
            $table->dropColumn('available');
        });
        Schema::table('users', function ($table) {
            $table->dropColumn('login_count');
        });
    }
}
