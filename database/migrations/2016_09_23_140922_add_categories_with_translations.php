<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoriesWithTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments("id")->index();
            $table->integer("parent")->unsigned()->nullable()->default(null);
            $table->timestamps();
        });

        Schema::create('category_translations', function (Blueprint $table) {
            $table->increments("id")->index();
            $table->integer("category_id")->unsigned();
            $table->string("language", 2);
            $table->string("phrase", 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
        Schema::drop('category_translations');
    }
}
