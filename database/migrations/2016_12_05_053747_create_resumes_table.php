<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resumes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->references('id')->on('user_id');
            $table->string('email')->nullable();
            $table->integer('birth_year')->nullable();
            $table->boolean('male')->nullable();
            $table->integer('high_school_type')->nullable();
            $table->string('high_school_name')->nullable();
            $table->integer('college_type')->nullable();
            $table->string('college_name')->nullable();
            $table->integer('occupation')->nullable();
            $table->integer('education')->nullable();
            $table->string('additional')->nullable();
            $table->string('hobbies')->nullable();
            $table->string('advantages')->nullable();
            $table->string('wish_occupation')->nullable();
            $table->string('wish_place')->nullable();
            $table->integer('wish_cost')->nullable();
            $table->integer('wish_contract')->nullable();
            $table->integer('wish_full_time')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resumes');
    }
}
