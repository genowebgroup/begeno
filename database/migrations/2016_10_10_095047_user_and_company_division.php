<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserAndCompanyDivision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function(Blueprint $table){
            $table->integer('user_id')->
                index()->
                after('id')->
                unsigned()->
                references('id')->on('users')->
                onDelete('cascade')->
                nullable()->default(null);
        });

        Schema::create('user_data', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->index()->unsigned();
            $table->String('name');
            $table->String('surname');
            $table->String('location');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function ($table) {
            $table->dropColumn('user_id');
        });

        Schema::drop('user_data');
    }
}
