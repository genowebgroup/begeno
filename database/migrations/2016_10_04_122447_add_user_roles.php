<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table){
           $table->increments('id')->index();
           $table->string('name');
           $table->timestamps();
        });
        foreach (['admin', 'company', 'individual'] as $roleName){
            $role = new \App\Models\Role();
            $role->name=$roleName;
            $role->save();
        }

        Schema::table('users', function (Blueprint $table){
            $table->integer("role_id")->nullable()->default(null)->after('remember_token')->unsigned()->references('id')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('role_id');
        });
        Schema::drop('roles');
    }
}
