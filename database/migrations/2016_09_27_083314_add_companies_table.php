<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments("id")->index();
            $table->string("name", 255)->nullable()->default("");
            $table->string("url", 2083)->nullable()->default("");
            $table->string("phone", 50)->nullable()->default("");
            $table->timestamps();
        });

        Schema::create('company_translations', function (Blueprint $table) {
            $table->increments("id")->index();
            $table->integer("company_id")->unsigned();
            $table->string("description");
            $table->string("language", 2);
            $table->timestamps();
        });

        Schema::create('category_company', function (Blueprint $table) {
            $table->integer('company_id')->unsigned()->index();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_company_translation');
        Schema::drop('companies');
        Schema::drop('category_company');
    }
}
