<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Mail::class, function (Faker\Generator $faker) {
    return [
        'sender_id' => $faker->numberBetween(1,4),
        'recipient_id' => $faker->numberBetween(1,4),
        'header' => $faker->sentence(3),
        'body' => $faker->text(500),
        'sent_at' => $faker->date(),
        'created_at' => $faker->date(),
        'updated_at' => $faker->date()
    ];
});

$factory->define(App\Models\MailMetadata::class, function (Faker\Generator $faker) {
    return [
        'user_id' => $faker->numberBetween(1,4),
        'status_id' => $faker->numberBetween(1, 4),
        'is_favorite' => $faker->boolean(15),
        'is_read' => $faker->boolean(90),
        'read_at' => $faker->date(),
        'created_at' => $faker->date(),
        'updated_at' => $faker->date()
    ];
});