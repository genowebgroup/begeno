<?php

use Illuminate\Database\Seeder;

class MailPopulateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Mail::class, 50)->create()->each(function($m){
            factory(App\Models\MailMetadata::class)->create(['user_id' => $m->recipient_id, 'mail_id' =>$m->id, 'created_at' => $m->send_at ]);
            factory(App\Models\MailMetadata::class)->create(['user_id' => $m->sender_id, 'mail_id' =>$m->id ]);
            //$m->metadata()->sync();
        });
    }
}
