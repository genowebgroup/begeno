@extends('layouts.metronic')


@section('content')
<div class="row">
    <div class="col-lg-8 col-xs-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> Curriculum Vitae:</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="resume" role="form" method="post" action="">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">
                        <div class="form-group">
                            <div class="input-group">
                            <input type="text" name="email" value="{{$resume ? $resume->email : ""}}">email<br/>
                            <input type="text" name="birth_year" value="{{$resume ? $resume->birth_year : ""}}">Rok urodzenia<br/>
                                <select name='male' form='resume'>
                                    <option value="0">Kobieta</option>
                                    <option value="1">Mężczyzna</option>
                                </select>Płeć<br/>
                                <select name='high_school_type' form='resume'>
                                    <option value="0">Liceum</option>
                                    <option value="1">Technikum</option>
                                    <option value="2">Zawodowa</option>
                                </select>Szkoła średnia<br/>
                            <input type="text" name="high_school_name"  value="{{$resume ? $resume->high_school_name : ""}}">Nazwa szkoły średniej<br/>
                                <select name='college_type' form='resume'>
                                    <option value="0">Szkola wyższa</option>
                                    <option value="1">szkoła policealna</option>
                                </select>Szkoła wyższa<br/>
                                <input type="text" name="college_name"  value="{{$resume ? $resume->college_name : ""}}">Nazwa szkoły wyższej<br/>
                                <select name='occupation' form='resume'>
                                    @foreach(\App\Models\Occupation::get() as $occupation)
                                    <option value="{{$occupation->id}}">{{$occupation->name}}</option>
                                        @endforeach
                                </select>Zawód<br/>

                                <select name='education' form='resume'>
                                    <option value="0">podstawowe</option>
                                    <option value="1">średnie</option>
                                    <option value="2">wyższe</option>
                                </select>Wykształcenie<br/>
                            <input type="text" name="additional"  value="{{$resume ? $resume->additional : ""}}">Szkolenia, języki<br/>
                            <input type="text" name="hobbies"  value="{{$resume ? $resume->hobbies : ""}}">Zainteresowania<br/>
                            <input type="text" name="advantages"  value="{{$resume ? $resume->advantages : ""}}">Mocne strony<br/>
                                <select name='wish_occupation' form='resume'>
                                    @foreach(\App\Models\Occupation::get() as $occupation)
                                        <option value="{{$occupation->id}}">{{$occupation->name}}</option>
                                    @endforeach
                                </select>   Oczekiwany zawód<br/>
                            <input type="text" name="wish_place"  value="{{$resume ? $resume->wish_place : ""}}">Miejsce oczekiwane<br/>
                            <input type="text" name="wish_cost" value="{{$resume ? $resume->wish_cost : ""}}">Oczekiwane zarobki<br/>
                                <select name='wish_contract' form='resume'>
                                    <option value="0">umowa o pracę</option>
                                    <option value="1">umowa zlecenie</option>
                                    <option value="2">umowa o dzieło</option>
                                </select>Oczekiwana forma zatrudnienia<br/>

                                <select name='wish_full_time' form='resume'>
                                    <option value="0">pełny</option>
                                    <option value="1">pół etatu</option>
                                </select>Wymiar zatrudnienia<br/>
                            </div>
                        <input type="submit" class="btn submit blue">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection