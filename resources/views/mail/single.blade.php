@extends('mail.mail')

                    @section('mail')


                        <div class="inbox-header inbox-view-header">
                            <h1 class="pull-left">
                                {{$message->mail->header}}
                                <a href="javascript:;"> Inbox </a>
                            </h1>
                            <div class="pull-right">
                                <a href="javascript:;" class="btn btn-icon-only dark btn-outline">
                                    <i class="fa fa-print"></i>
                                </a>
                            </div>
                        </div>
                        <div class="inbox-view-info">
                            <div class="row">
                                <div class="col-md-7">
                                    <img src="/assets/pages/media/users/avatar1.jpg" class="inbox-author">
                                    <span class="sbold">{{$message->mail->sender_id}} </span>
                                    <span>&#60;support@go.com&#62; </span> to
                                    <span class="sbold"> me </span>  </div>
                                <div class="col-md-5 inbox-info-btn">
                                    <div class="btn-group">
                                        <button data-messageid="23" class="btn green reply-btn" onclick="location.href='/mail/new/{{$message->mail->sender_id}}';">
                                            <i class="fa fa-reply"></i> Odpowiedz
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="javascript:;" data-messageid="23" class="reply-btn">
                                                    <i class="fa fa-reply"></i> Reply </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-arrow-right reply-btn"></i> Forward </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-print"></i> Print </a>
                                            </li>
                                            <li class="divider"> </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-ban"></i> Spam </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-trash-o"></i> Delete </a>
                                            </li>
                                            <li>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="inbox-view">
                            {{$message->mail->body}}
                        </div>
                        <hr>
                        @if (!$message->mail->attachments->isEmpty())
                        <div class="inbox-attached">
                            <div class="margin-bottom-25">
                                <div>
                                    <strong>image4.jpg</strong>
                                    <span>173K </span>
                                    <a href="{{Storage::disk()->url($message->mail->attachments->first()->location_name)}}">View </a>
                                    <a href="javascript:;">Download </a>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endsection