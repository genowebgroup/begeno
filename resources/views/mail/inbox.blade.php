@extends('mail.mail')

                    @section('mail')
                        <div class="inbox-content">
                        <table class="table table-striped table-advance table-hover">
                            <thead>
                            <tr>
                                <th colspan="3">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="mail-group-checkbox" />
                                        <span></span>
                                    </label>
                                    <div class="btn-group input-actions">
                                        <a class="btn btn-sm blue btn-outline dropdown-toggle sbold" href="javascript:;" data-toggle="dropdown"> Akcja
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-pencil"></i> Oznacz jako przeczytaną </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-ban"></i> Spam </a>
                                            </li>
                                            <li class="divider"> </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-trash-o"></i> Usuń </a>
                                            </li>
                                        </ul>
                                    </div>
                                </th>
                                <th class="pagination-control" colspan="3">
                                    <span class="pagination-info"> 1-30 of 789 </span>
                                    <a class="btn btn-sm blue btn-outline">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a class="btn btn-sm blue btn-outline">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($mails as $mail)
                            <tr {!! $mail->is_read ? "" : "class='unread'"!!} data-messageid="1">
                                <td class="inbox-small-cells">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="mail-checkbox" value="1" />
                                        <span></span>
                                    </label>
                                </td>
                                <td class="inbox-small-cells">
                                    <i class="fa fa-star {{$mail->is_favorite ? ' inbox-started' : ""}}"></i>
                                </td>
                                <td class="view-message hidden-xs"> {{str_limit($mail->mail->sender->email, 20, "...")}} </td>
                                <td class="view-message "> <a href="/mail/message/{{ $mail->id}}">{{str_limit($mail->mail->header, 40, "...")}} </a></td>
                                <td class="view-message inbox-small-cells">
                                    {!! $mail->mail->attachments->isEmpty() ? '' : '<i class="fa fa-paperclip"></i>'  !!}
                                </td>
                                <td class="view-message text-right"> {{$mail->mail->sent_at}} </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    @endsection