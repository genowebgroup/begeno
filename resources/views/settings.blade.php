@extends('layouts.metronic')

@section('content')

@if(Auth::user() && \App\Models\Company::where('id', $id)->first()->user_id == Auth::user()->id)
    <div class="portlet-body form">
        <form role="form" method="post" action="">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-body">
                <div class="form-group">
                    <label>Wybierz subdomenę</label>
                    <div class="input-group">
                        <input type="hidden" name="action" value="subdomain">
                        <input type="text" name="title" class="form-control" placeholder="subdomain">.begeno.com </div>
                </div>
                <input type="submit" class="btn submit blue">
            </div>
        </form>
    </div>
@endif()
@endsection

@section('pageLevelPluginsJS')
@parent
<script src="../assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
@endsection