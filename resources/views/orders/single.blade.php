@extends("layouts.metronic")


@section("content")

    <div class="col-lg-10">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Zlecenia</span>
                    <div class="caption-desc font-grey-cascade"> Filtrowane według twoich preferencji i kwalifikacji </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="mt-element-list">
                    <div class="mt-list-head list-default green-haze">
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="list-head-title-container">
                                    <h3 class="list-title uppercase sbold">Dostępne</h3>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="list-head-summary-container">
                                    <div class="list-pending">
                                        <div class="badge badge-default list-count">3</div>
                                        <div class="list-label">Interesują mnie</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-list-container list-default">
                        <ul>
                            @foreach($orders as $order)
                            <li class="mt-list-item">
                                <div class="list-icon-container done">
                                    <a href="javascript:;">
                                        <i class="icon-check"></i>
                                    </a>
                                </div>
                                <div class="list-datetime"> {{$order->cost}} zł
                                    <br> <span>{{Carbon\Carbon::parse($order->deadline)->format('d.m.Y')}}</span>
                                    <br><br> <span>{{$order->location}}</span> </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase bold">
                                        <a href="javascript:;">{{$order->title}}</a>
                                    </h3>
                                    <p>{{$order->description}}</p><br>
                                    <form action="/mail/new/{{$order->user_id}}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="title" value="Jestem zainteresowany ogłoszeniem '{{$order->title}}'">
                                        <p><input type="submit" value="Jestem zainteresowany wykonaniem"></p>
                                    </form>
                                </div>
                            </li>
                            @endforeach
                            @if(false)
                            <li class="mt-list-item">
                                <div class="list-icon-container done">
                                    <a href="javascript:;">
                                        <i class="icon-check"></i>
                                    </a>
                                </div>
                                <div class="list-datetime"> 8000 zł
                                    <br> <span>18.12.2016</span>
                                    <br><br> <span>Gdańsk</span> </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase bold">
                                        <a href="javascript:;">Wizytówka firmy - wordpress</a>
                                    </h3>
                                    <p>Zatrudnię programistę PHP do wykonania prostej strony</p><br>
                                    <p><span class="label label-info"> Programowanie</span></p>
                                </div>
                            </li>
                            <li class="mt-list-item">
                                <div class="list-icon-container">
                                    <a href="javascript:;">
                                        <i class="icon-close"></i>
                                    </a>
                                </div>
                                <div class="list-datetime"> 11am
                                    <br> 8 Nov </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase bold">
                                        <a href="javascript:;">Listings Feature</a>
                                    </h3>
                                    <p>Lorem ipsum dolor sit amet</p>
                                </div>
                            </li>
                            <li class="mt-list-item">
                                <div class="list-icon-container">
                                    <a href="javascript:;">
                                        <i class="icon-close"></i>
                                    </a>
                                </div>
                                <div class="list-datetime"> 11am
                                    <br> 8 Nov </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase bold">
                                        <a href="javascript:;">Listings Feature</a>
                                    </h3>
                                    <p>Lorem ipsum dolor sit amet</p>
                                </div>
                            </li>
                            <li class="mt-list-item">
                                <div class="list-icon-container done">
                                    <a href="javascript:;">
                                        <i class="icon-check"></i>
                                    </a>
                                </div>
                                <div class="list-datetime"> 11am
                                    <br> 8 Nov </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase bold">
                                        <a href="javascript:;">Listings Feature</a>
                                    </h3>
                                    <p>Lorem ipsum dolor sit amet</p>
                                </div>
                            </li>
                            <li class="mt-list-item">
                                <div class="list-icon-container">
                                    <a href="javascript:;">
                                        <i class="icon-close"></i>
                                    </a>
                                </div>
                                <div class="list-datetime"> 11am
                                    <br> 8 Nov </div>
                                <div class="list-item-content">
                                    <h3 class="uppercase bold">
                                        <a href="javascript:;">Listings Feature</a>
                                    </h3>
                                    <p>Lorem ipsum dolor sit amet</p>
                                </div>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>






@endsection