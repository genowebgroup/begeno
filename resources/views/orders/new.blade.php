@extends("layouts.metronic")


@section("content")

    <div class="row">
        <div class="col-lg-8 col-xs-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Company info:</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form id='order' role="form" method="post" action="">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-body">
                            <div class="form-group">
                                <label>Tytuł zlecenia</label>
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-align-justify"></i>
                                </span>
                                    <input type="text" name="title" class="form-control" placeholder="Your first name"> </div>
                            </div>
                            <div class="form-group">
                                <label>Opis</label>
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </span>
                                    <textarea type="text" name="description" class="form-control" placeholder="Your second name"> </textarea></div>
                            </div>
                            <div class="form-group">
                                <label>Miejsce</label>
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-newspaper-o"></i>
                                </span>
                                    <input type="text" name="location" class="form-control" placeholder="Your location"> </div>
                            </div>

                            <div class="form-group">
                                <label>Budżet</label>
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-newspaper-o"></i>
                                </span>
                                    <input type="text" name="cost" class="form-control" placeholder="Your location"> </div>
                            </div>

                            <input type="hidden" name="deadline" value="2016-11-29">

                            <div class="form-group">
                                <label>Branża</label>
                                <select name='category_id' form='order' class="form-control">
                                    @foreach($categories as $category)
                                    <option value="{{$category->category_id}}">{{$category->phrase}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="submit" class="btn submit blue">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection