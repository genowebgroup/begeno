@extends('layouts.metronic')

@section('sidebar')
    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
        <li class="nav-item start active open">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Panel</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                @foreach(['Pulpit', 'mój profil', 'dane firmy', 'moje produkty', 'moje usługi', 'artykuły'] as $k)
                <li class="nav-item  ">
                    <a href="" class="nav-link ">
                        <i class="icon-bar-chart"></i>
                        <span class="title">{{$k}}</span>
                    </a>
                </li>
                @endforeach
            </ul>
        </li>
    </ul>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-8 col-xs-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <div class="row">
                    @foreach(["POCZTA" => 'mail', "FIRMY" => 'companies', "FAKTURY" => '/1', "ZLECENIA" => '/1', "KONTRAHENCI" => '/1', "PROMOCJE" => '/1',] as $k => $j)
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 bordered" onclick="window.location='/{{$j}}';"  style="cursor:pointer;">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-green-sharp">
                                        <span >{{$k}}</span>
                                    </h3>
                                    <small>{{$k}}</small>
                                </div>
                                <div class="icon">
                                    <i class="icon-pie-chart"></i>
                                </div>
                            </div>
                            <div class="progress-info">
                                <div class="progress">
                                        <span style="width: 100%;" class="progress-bar progress-bar-success green-sharp">
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
    @section('insidecontent')
    @show
</div>
@endsection