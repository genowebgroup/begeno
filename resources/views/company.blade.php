@extends('layouts.metronic')

@section('content')
<div class="row">
    <div class="col-lg-8 col-xs-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold uppercase font-dark">{{$company->name}}</span>
                </div>
            </div>
            <div class="portlet-body">
                <div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_1" > Firma </a>
                        </li>
                        <li >
                            <a href="{{$company->id}}/articles" > Artykuły</a>
                        </li>
                        <li>
                            <a href="{{$company->id}}/services" > Usługi </a>
                        </li>
                        @if(0)
                        <li>
                            <a href="#tab_1_2" data-toggle="tab"> Zarządzaj </a>
                        </li>
                        @endif
                    </ul>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <a href="javascript:;" class="thumbnail">
                            <img src="https://placehold.it/200x200">
                        </a>
                    </div>
                    <div>
                    </div>
                    <div class="col-md-6">
                        <h3>Contact</h3>
                        <div class="well">
                            <address>
                                <strong>Phone: </strong>
                                {{$company->phone}}
                            </address>
                            <address>
                                <strong>Website: </strong>
                                {{$company->url}}
                            </address>
                            <address>
                                <strong>e-mail addres: </strong>
                                <a href="mailto:#">{{$company->email}}</a>
                            </address>
                            @if($company->type != 0)
                            <address>
                                <strong>Opis: </strong>
                                {{$company->description}}
                            </address>
                            @endif
                        </div>
                    </div>
                </div>
                @foreach($company->categories as $category)
                    <span class="label label-info"> {{$category->translation('pl')->first()->value()}}</span>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection