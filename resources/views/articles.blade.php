@extends('layouts.metronic')

@section('content')

@if(Auth::user() && \App\Models\Company::where('id', $id)->first()->user_id == Auth::user()->id)
    <div class="portlet-body form">
        <form role="form" method="post" action="">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-body">
                <div class="form-group">
                    <label>nowy artykuł</label>
                    <div class="input-group">
                        <input type="text" name="title" class="form-control" placeholder="title"> </div>

                    <textarea class="inbox-editor inbox-wysihtml5 form-control" name="body" rows="12"></textarea>
                </div>
                <input type="submit" class="btn submit blue">
            </div>
        </form>
    </div>
@endif()
@foreach($articles as $article)
<div class="row">
    <div class="col-lg-8 col-xs-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold uppercase font-dark">
                        <a href="article/{{$article->id}}">
                        {{$article->title}}</a></span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="well">
                            <address>
                                {{$article->body}}
                            </address>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    @endforeach
@endsection

@section('pageLevelPluginsJS')
@parent
<script src="../assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
@endsection