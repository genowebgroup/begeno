@extends('layouts.metronic')


@section('content')
<div class="row">
    <div class="col-lg-8 col-xs-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> Company info:</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" method="post" action="">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Name</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-align-justify"></i>
                                </span>
                                <input type="text" name="name" class="form-control" placeholder="Your first name"> </div>
                        </div>
                        <div class="form-group">
                            <label>Surname</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </span>
                                <input type="text" name="phone" class="form-control" placeholder="Your second name"> </div>
                        </div>
                        <div class="form-group">
                            <label>Location</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-newspaper-o"></i>
                                </span>
                                <input type="text" name="url" class="form-control" placeholder="Your location"> </div>
                        </div>
                        <input type="submit" class="btn submit blue">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection