<?php
    $Path = 'assets/img/';
    $i = 0;
    foreach (glob($Path."*.jpg") as $filename) {
        $i++;
        $images[$i] = $filename ;
        //echo $filename;
    }
    $j = rand(1,$i);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.ico">

    <title>begeno :: portal dla każdego</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/ionicons.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        html, body {
            overflow: hidden;
        }
        #h {
            background: url(<?php echo $images[$j];?>) no-repeat center top;
            padding-top: 180px;
            text-align:center;
            background-attachment: relative;
            background-position: center center;
            min-height: 700px;
            width: 100%;
            color: white;

            -webkit-background-size: 100%;
            -moz-background-size: 100%;
            -o-background-size: 100%;
            background-size: 100%;

            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
</head>

<body>

<div id="h">
    <div class="logo"><span style="padding:8px;background-color:#9C9C9C;color:#ffffff;font-family: 'Calibri Light';font-size: 24px;opacity: 0.75; filter: alpha(opacity=75);">begeno.com</span></div>
    <div class="social hidden-xs">
        @if (Route::has('login'))
            <div class="top-right links">
                <a href="{{ url('/login') }}">zaloguj</a>
                &nbsp;&nbsp;&nbsp;
                <a href="{{ url('/register') }}">rejestruj</a>
            </div>
        @endif
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 centered">
                <div class="mtb">
                    <form role="form" action="search.php" method="post" enctype="plain">
                        <input type="email" name="email" class="subscribe-input" placeholder="wpisz frazę..." required>
                        <button class='btn btn-conf btn-green' type="submit">Szukaj</button>
                    </form>
                </div><!--/mt-->
            </div>

        </div>

        <div class="row">

            <table width="100%" align="center" style="margin-top: 128px">
                <tr>
                    <td valign="top" align="left" style="color:dimgrey;background-color: white;opacity: 0.75; filter: alpha(opacity=75);height: 160px">
                        <br>
                        <?php readfile("http://kursy-walut.com/pp/pokaz_kursy_30_utf.php"); ?>

                    </td>
                    <td valign="top" align="left" style="color:dimgrey;background-color: white;opacity: 0.75; filter: alpha(opacity=75);height: 160px">

                        <?php readfile("http://kursy-walut.com/pp/wykresy/pokaz_wykresy_indeksow_20_utf.php?indeksy=WIG20"); ?>

                    </td>
                    <td valign="top" align="left" style="color:dimgrey;background-color: white;opacity: 0.75; filter: alpha(opacity=75);height: 160px">

                        <?php
                        $url = "http://www.money.pl/rss/gospodarka.xml";
                        $i = 0; // counter

                        $rss = simplexml_load_file($url); // XML parser

                        // RSS items loop

                        //print '<h2><img style="vertical-align: middle;" src="'.$rss->channel->image->url.'" /> '.$rss->channel->title.'</h2>'; // channel title + img with src

                        foreach($rss->channel->item as $item) {
                            if ($i < 8) { // parse only 10 items
                                $url_news = $item->link ;
                                $long_title = $item->title ;
                                $title = substr($item->title, 0, 44);
                                $title .="...";
                                $url_news = str_replace('beta.gegeno.com/', '', $url_news);
                                $url_news = str_replace('homestead.app/', '', $url_news);
                                echo '<a href=\"'.$url_news.'\" target=\"_blank\" style="font-family: Calibri;font-size: 11px;color:black;margin-left: 4px;margin-top: 4px">'.$title.'</a><br />';
                            }

                            $i++;
                        }

                        ?>

                    </td>
                    <td width="16">
                        &nbsp
                    </td>
                    <td valign="top" align="left" style="width:512px;color:dimgrey;background-color: white;opacity: 0.75; filter: alpha(opacity=75);height: 160px">
                        <span style="margin-left: 16px;margin-top: 4px;">
                        wiadomości:<br>

                        <?php
                            $url = "http://tvn24bis.pl/najnowsze.xml";
                        $i = 0; // counter

                        $rss = simplexml_load_file($url); // XML parser

                        // RSS items loop

                        //print '<h2><img style="vertical-align: middle;" src="'.$rss->channel->image->url.'" /> '.$rss->channel->title.'</h2>'; // channel title + img with src

                        foreach($rss->channel->item as $item) {
                            if ($i < 7) { // parse only 10 items
                                $url_news = $item->link ;
                                $url_news = str_replace('beta.gegeno.com/', '', $url_news);
                                $url_news = str_replace('homestead.app/', '', $url_news);
                                echo '<a href=\"'.$url_news.'\" target=\"_blank\" style="font-family: Calibri;font-size: 14px;color:black;margin-left: 16px;margin-top: 4px">'.$item->title.'</a><br />';
                            }

                            $i++;
                        }

                        ?>
                        </span>
                    </td>

                </tr>
            </table>

        </div><!--/row-->
    </div><!--/container-->

</div><!-- /H -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/retina-1.1.0.js"></script>
</body>
</html>
