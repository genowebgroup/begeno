@extends('layouts.metronic')


@section('content')
    <script>
        function validateForm() {
            //return false;
            var x = document.forms["company"]["phone"].value;

            if (x.toString().length != 9) {
                alert("Numer telefonu musi mieć 9 cyfr");
                return false;
            }
//            alert(x.match(/^\d+$/))
            if (!x.match(/^\d+$/)) {
                alert("Numer telefonu musi być liczbą");
                return false;
            }
            //alert(x);
        }
    </script>
<div class="row">
    <div class="col-lg-8 col-xs-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> Company info:</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form name="company" role="form"  onsubmit="return validateForm()" method="post" action="">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Company name</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-align-justify"></i>
                                </span>
                                <input type="text" name="name" class="form-control" placeholder="Registered name of the company" value="{{$company->name or ''}}"> </div>
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </span>
                                <input type="text" name="phone" class="form-control" placeholder="Telephone number" value="{{$company->phone or ''}}"> </div>
                        </div>
                        <div class="form-group">
                            <label>Website</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-newspaper-o"></i>
                                </span>
                                <input type="text" name="url" class="form-control" placeholder="Web page of the company" value="{{$company->url or ''}}"> </div>
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <input type="text" name="email" class="form-control" placeholder="Email Address" value="{{$company->email or ''}}"> </div>
                        </div>
                        <input type="hidden" name="id" value="{{$company->id or ''}}">
                        <input type="submit" class="btn submit blue">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection