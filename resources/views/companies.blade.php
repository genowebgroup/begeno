@extends('layouts.metronic')

@section('content')
@if(Auth::user() && Auth::user()->role == null)
<div class="col-lg-8 col-xs-12 col-sm-12 alert alert-block alert-warning fade in"">
    <button type="button" class="close" data-dismiss="alert"></button>
    <h4 class="alert-heading">One more thing!</h4>
    <p> You are registered but we don't know if you're an individual user or a company. </p>
    <p> Choose your account type to get full access to this website's content </p>
    <p>
        <a class="btn blue" href="{{route('editProfile')}}"> I'm an individual user! </a>
        <a class="btn red" href="{{route('editCompany')}}"> I'm a company! </a>
    </p>
</div>
@endif
@if(Auth::user() && Auth::user()->active == 0)
<div class="col-lg-8 col-xs-12 col-sm-12 alert alert-block alert-warning fade in"">
    <button type="button" class="close" data-dismiss="alert"></button>
    <h4 class="alert-heading">One more thing!</h4>
    <p> Your account is not confirmed yet! </p>
    <p> Check your email for confirmation token (it's probably in spam folder)! </p>
    <p>
        <a class="btn blue" href="{{route('editProfile')}}"> Resend mail! </a>
    </p>
</div>
@endif
@foreach($companies as $company)
<div class="row">
    <div class="col-lg-8 col-xs-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold uppercase font-dark">
                        <a href="company/{{$company->id}}">
                        {{$company->name}}</a></span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-sm-3 col-md-3">
                        <a href="javascript:;" class="thumbnail">
                            <img src="https://placehold.it/150x150">
                        </a>
                    </div>
                    <div class="col-md-6">
                        <h3>Contact</h3>
                        <div class="well">
                            <address>
                                <strong>Phone: </strong>
                                {{$company->phone}}
                            </address>
                            <address>
                                <strong>Website: </strong>
                                {{$company->url}}
                            </address>
                            <address>
                                <strong>e-mail addres: </strong>
                                <a href="mailto:#">{{$company->email}}</a>
                            </address>
                            @if ( $company->user_id )
                            <address>
                                <strong>contact: </strong>
                                <a href="/mail/new/{{$company->user_id}}">Click to send Message!</a>
                            </address>
                            @endif
                        </div>
                    </div>
                </div>
                @foreach($company->categories as $category)
                    <span class="label label-info"> {{$category->translation('pl')->first()->value()}}</span>
                @endforeach
            </div>
        </div>
    </div>
</div>
    @endforeach
@endsection

@section('pageLevelPluginsJS')
@parent
<script src="../assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
@endsection