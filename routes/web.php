<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\Models\Company;
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('landing');
})->name('landing');

Route::get('/number/{number}', function($number) {
    $company = App\Models\Company::where('phone', $number)->first();
    //var_dump($company);die();
    if($company){
        $company_id = $company->id;
        var_dump($company_id);
        return redirect()->route('singleCompany', ['company' => $company_id]);
    }else{
        return redirect()->route('landing');
    }
});

Route::get('/front', function(){
    return view('front');
});

Route::get('/page', function(){
    return view('standard');
});

Route::get('/landing', function(){
    return view('landing');
});

Auth::routes();

Route::get('/confirm/{token}', "Auth\ConfirmEmailController@index");

Route::get('/login2', function (){
    return view('auth.login2');
});

Route::get('/home', 'HomeController@index');

Route::get('/companies', "CompanyViewController@all");

Route::get('/user/edit', "UserController@edit")->name('editProfile');

Route::get('/company/edit', "CompanyViewController@edit")->name('editCompany');

Route::post('/company/edit', function(Request $request){
    //return 1;
    if($request->input('id') == '')
        $company = new \App\Models\Company();
    else
        $company = \App\Models\Company::find($request->input('id'));
    $company->user_id = Auth::user()->id;
    $company->name = $request->input('name');
    $company->phone = $request->input('phone');
    $company->url = $request->input('url');
    $company->email = $request->input('email');
    $company->save();
   return "Success";

});

Route::get('/company/{company}', "CompanyViewController@index")->name('singleCompany');
Route::get('/company/{company}/settings', "CompanyViewController@settings");
Route::post('/company/{company}/settings', function(Request $request){
    $post = new \App\Models\Subdomain();
    $post->name = $request->input('title');
    $post->user_id = Auth::user()->id;
    $post->save();
});
Route::get('/company/{id}/articles', "CompanyViewController@articles")->name('articles');
Route::post('/company/{company}/articles', function(Request $request){
    $post = new \App\Models\Post();
    $post->title = $request->input('title');
    $post->body = $request->input('body');
    $post->user_id = Auth::user()->id;
    $post->save();
});
Route::get('/panel', function(){
    $user = Auth::user();
    if($user->role && $user->role->id == \App\Enums\Roles::Company)
        return view('panel');
    return redirect('/companies');
});

Route::post('/mail/new', function(Request $request){
    $mail = new \App\Models\Mail();
    $mail->sender_id = Auth::user()->id;
    $mail->recipient_id = $request->input('recipientId');
    $mail->body = $request->input('message');
    $mail->header = $request->input('subject');
    $mail->sent_at = \Carbon\Carbon::now();
    $mail->save();
    //var_dump($mail->id);

    $mailMeta = new \App\Models\MailMetadata();
    $mailMeta->mail_id = $mail->id;
    $mailMeta->user_id = $request->input('recipientId');
    $mailMeta->save();

    $mailMeta = new \App\Models\MailMetadata();
    $mailMeta->mail_id = $mail->id;
    $mailMeta->user_id = Auth::user()->id;
    $mailMeta->save();

    $attachment_location = $request->file('attachment')->store("public/attachments");
    var_dump($attachment_location);
    $file = $request->file('attachment');
    var_dump($file);
    $attachment_real_name = $file->getClientOriginalName();
    var_dump($attachment_real_name);
    $attachment = new \App\Models\Attachment();
    $attachment->real_name = $attachment_real_name;
    $attachment->location_name = $attachment_location;
    $attachment->mail_id = $mail->id;
    $attachment->save();


});

Route::get('/mail/sent/{page}', "MailController@sent");
Route::get('/mail/sent', "MailController@sent");
Route::get('/mail/trash/{page}', "MailController@trash");
Route::get('/mail/trash', "MailController@trash");
Route::get('/mail/draft/{page}', "MailController@draft");
Route::get('/mail/draft', "MailController@draft");
Route::get('/mail/new', "MailController@create");
Route::get('/mail/new/{id}', "MailController@create");
Route::post('/mail/new/{id}', "MailController@create2");
Route::get('/mail/message/{mail}', "MailController@single");
Route::get('/mail/reply/{mail}', "MailController@create");
Route::get('/mail/{page}', "MailController@inbox");
Route::get('/mail', "MailController@inbox");



Route::get('/order/new', function (){
    $categories = \App\Models\CategoryTranslation::where('language', "pl")->get();
    return view('orders.new')->with('categories', $categories);
});

Route::post('/order/new', function (Request $request){
    $order = new \App\Models\Order();
    $order->title = $request->input('title');
    $order->description = $request->input('description');
    $order->location =$request->input('location');
    $order->cost =$request->input('cost');
    $order->category_id =$request->input('category_id');
    $order->deadline =$request->input('deadline');
    $order->user_id =Auth::user()->id;
    $order->save();
    var_dump($request->input('description'));
});
Route::get('/service/new', function (){
    $categories = \App\Models\CategoryTranslation::where('language', "pl")->get();
    return view('services.new')->with('categories', $categories);
});

Route::post('/service/new', function (Request $request){
    $service = new \App\Models\Service();
    $service->title = $request->input('title');
    $service->description = $request->input('description');
    $service->location =$request->input('location');
    $service->cost =$request->input('cost');
    $service->user_id =Auth::user()->id;
    $service->save();
    var_dump($request->input('description'));
});

Route::get('/order/{id}', function($id){
    $orders = App\Models\Order::limit(30)->offset(30)->get();
    return view('orders.single')->with('orders', $orders);
});

Route::get('/orders/mine', function(){

});
Route::get('/orders/forme', function(){

    $user = Auth::user();
    if($user->role && $user->role->id == \App\Enums\Roles::Company){
        $company = Company::where('user_id', $user->id)->get();
 /*
        var_dump($company);
        echo "<br/><br/><br/>";
        var_dump($company->first());
        echo "<br/><br/><br/>";
        var_dump($company->first()->categories->first()->id);
*/
        $orders = App\Models\Order::where("category_id", $company->first()->categories->first()->id)->limit(30)->get();
        return view('orders.single')->with('orders', $orders);
    }else{
        return redirect()->route('landing');
    }
});

Route::get('/orders', function (){
    return view('orders.index');
});

Route::get('/service/{id}', function($id){
    $services = App\Models\Service::limit(30)->get();
    return view('services.single')->with('services', $services);
});

Route::get('/company/{id}/services', function($id){
    $services = App\Models\Service::where('user_id', Company::find($id)->user_id)->limit(30)->get();
    return view('services.single')->with('services', $services);
});

Route::get('/services/mine', function(){

});
Route::get('/services/forme', function(){

    $user = Auth::user();
    if($user->role && $user->role->id == \App\Enums\Roles::Company){
        $company = Company::where('user_id', $user->id)->get();
 /*
        var_dump($company);
        echo "<br/><br/><br/>";
        var_dump($company->first());
        echo "<br/><br/><br/>";
        var_dump($company->first()->categories->first()->id);
*/
        $services = App\Models\Order::where("category_id", $company->first()->categories->first()->id)->limit(30)->get();
        return view('services.single')->with('services', $services);
    }else{
        return redirect()->route('landing');
    }
});

Route::get('/services', function (){
    return view('services.index');
});

Route::get('/resume', function (){
   return view("resume")->with("resume", false);
});
Route::post('/resume', function (Request $request){
   $inputs = ["email",
"birth_year",
"male",
"high_school_type",
"high_school_name",
"college_type",
"college_name",
"occupation",
"education",
"additional",
"hobbies",
"advantages",
"wish_occupation",
"wish_place",
"wish_cost",
"wish_contract",
"wish_full_time"];
   $resume = new \App\Models\Resume();
   foreach ($inputs as $input){
       if($request->input($input))
       $resume->$input =$request->input($input);
       echo $request->input($input);
   }
    $resume->user_id = Auth::user()->id;
   $resume->save();
    //return view("resume")->with("resume", false);
});
