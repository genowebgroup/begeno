<?php

namespace App\Http\Controllers;


use App\Enums\Roles;
use App\Http\Requests;
use App\Models\Company;
use App\Models\Post;
use Redirect;

class CompanyViewController extends Controller
{
    function index($id){
        $company = Company::find($id);
        $company->categories = $company->categories()->get();
        //dd($company);
        return view('company')->with("company", $company);
    }

    function articles($id){
        $user_id = Company::where('id', $id)->first()->user_id;
        var_dump($user_id);echo "laallala";
        $articles = Post::where('user_id', $user_id)->get();

        return view('articles')->with("articles", $articles)->with("id", $id);
    }

    function settings($id){
        return view('settings')->with("id", $id);
    }

    function all(){
        $companies = Company::with('categories')->get();
        //return $companies;
        return view('companies')->with("companies", $companies);
    }

    function edit(){
        $user = \Auth::user();
        if($user) {
            if ($user->role == null) {
                $user->setRole(Roles::Company);
                $user->save();
                $user = $user->fresh(['role']);
            }
            if ($user->role->id == Roles::Company) {
                $company = Company::where('user_id', $user->id)->first();
                return view('newcompany')->with('company', $company);
            }
        }
        return Redirect::to('/home');
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $name = $request->input('name');
        var_dump($name);

        //
    }
}
