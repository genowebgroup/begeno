<?php

namespace App\Http\Controllers;

use App\Enums\Roles;
use Illuminate\Http\Request;

use App\Http\Requests;
use Redirect;

class UserController extends Controller
{
    function edit(){
        $user = \Auth::user();
        if ($user->role == NULL){
            $user->setRole(Roles::User);
            $user->save();
            $user = $user->fresh(['role']);
        }
        if ($user->role->id == Roles::User)
            return view('editprofile');
        return Redirect::to('/home');
    }
}
