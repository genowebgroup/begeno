<?php

namespace App\Http\Controllers\Auth;

use App\Models\Confirmation;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ConfirmEmailController extends Controller
{
    function index($token){
        $confirmation = Confirmation::where('token', $token)->first();
        $username = $confirmation->email;
        $user = User::where('email', $username)->first();
        $user->active = true;
        $user->save();
        echo $username;
        return view('auth.confirm', ['username' => $username]);
    }
}
