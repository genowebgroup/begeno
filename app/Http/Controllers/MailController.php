<?php

namespace App\Http\Controllers;

use App\Models\Mail;
use App\Models\MailMetadata;
use Faker\Provider\cs_CZ\DateTime;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MailController extends Controller
{
    public function inbox($page = 1){
        $currentUserId = Auth::user()->id;
        //$mails = Mail::where('recipient_id', $currentUserId)->orderBy('sent_at', 'DESC')->get();
        $mails = MailMetadata::where('user_id', $currentUserId)->orderBy('created_at', 'DESC')->take(10)->get();
        return view("mail.inbox")->with('mails', $mails);
    }

    public function sent($page = 1){
        $currentUserId = Auth::user()->id;
        $mails = Mail::where('sender_id', $currentUserId)->orderBy('sent_at', 'DESC')->get();

        $mails = MailMetadata::where('user_id', $currentUserId)->orderBy('created_at', 'DESC')->take(10)->get();
        return view("mail.inbox")->with('mails', $mails);
    }

    public function trash($page = 1){
        $mails = Mail::where('sender_id', 1)->where("is_trash", 1)->orderBy('sent_at', 'DESC')->get();
        return view("mail.inbox")->with('mails', $mails);
    }

    public function draft($page = 1){
        $mails = Mail::where('sender_id', 1)->where("is_sent", 0)->orderBy('sent_at', 'DESC')->get();
        return view("mail.inbox")->with('mails', $mails);
    }

    public function create($recipientId = 1){
        return view("mail.new")->with('recipientId', $recipientId);
    }

    public function create2($recipientId = 1, Request $request){
        //var_dump($request);
        return view("mail.new")->with('recipientId', $recipientId)->with('request', $request);
    }

    public function single($messageId = 1){
        $message = \App\Models\MailMetadata::find($messageId);
        return view("mail.single")->with('message', $message);
    }
}
