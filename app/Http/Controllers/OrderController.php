<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class OrderController extends Controller
{
    public function add(){
        $user = \Auth::user();
        if($user) {

        }
    }

    public function index(){
        $orders = Order::paginate(10);
        return view("orders.index")->with("orders", $orders);
    }

    public function editAll(){
        $user = \Auth::user();
        if($user) {
            $orders = Order::where('user_id', $user->id)->paginate(10);
            return view("orders.index")->with("orders", $orders);
        }
    }

    public function edit($id){

    }

    public function single($id){
        $order = Order::find($id);
        $order_owner = User::find($order->user_id);
        $order_owner_name = $order_owner->name();
        //die();
        /*
        var_dump($order);
        echo "<br>";
        var_dump($order_owner);
        echo "<br>";
        var_dump($order_owner_name);
        echo "<br>";
        var_dump($id);*/
        return view("orders.single")->with("order", $order);
    }
}
