<?php
/**
 * Created by PhpStorm.
 * User: zkisly
 * Date: 2016-10-11
 * Time: 13:47
 */

namespace App\Enums;

final class Roles
{
    const __default = self::User;

    const Admin = 1;
    const Company = 2;
    const User = 3;
}