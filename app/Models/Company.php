<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function translations(){
        return $this->hasMany(CompanyTranslation::class);
    }

    public function translation($language){
        return $this->hasMany(CompanyTranslation::class)->where('language', $language);
    }

    public function categories(){
        return $this->belongsToMany(Category::class)->with("translations");
    }

    public function addCategory($id){
        //$pivot = new \App\Models\C
    }

    public function updateBarometer(){
        $result = 0;
        $first = User::find($this->user_id)->login_count;
        $first *= 25;
        $first /= 180;

        $complete = $this->name && $this->url &&$this->phone &&$this->email;

        $second = $complete ? 25 : 0;

        $third = Post::where('user_id', $this->user_id)->count();

        $fourth = $this->available ? 25 : 0; //is available

        $result=$first+$second+$third+$fourth;
        echo $first;
        echo $second;
        echo $this->name ;
        echo $this->url ;
        echo $this->phone ;
        echo $this->email ;
        //die();
        $this->barometer=$result+400;
        $this->save();
    }


}
