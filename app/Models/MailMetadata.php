<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class MailMetadata extends Model
{
    protected $table = 'mail_metadata';

    function mail(){
        return $this->belongsTo(Mail::class);
    }

    function user(){
        return $this->belongsTo(User::class);
    }
}
