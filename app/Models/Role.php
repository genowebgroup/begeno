<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed name
 */
class Role extends Model
{
    function Users(){
        return $this->hasMany(User::class);
    }
}
