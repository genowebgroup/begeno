<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Category extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token', 'email', 'activated',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function translations(){
        return $this->hasMany(CategoryTranslation::class);
    }

    public function translation($language){
        return $this->hasMany(CategoryTranslation::class)->where('language', $language);
    }

    public function companies(){
        return $this->belongsToMany(Company::class);
    }

    function get($language){

    }
}
