<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    function sent($page){

    }

    function inbox($page){

    }

    function trash($page){

    }

    function draft($page){

    }

    function metadata(){
        return $this->hasMany(MailMetadata::class);
    }

    function attachments(){
        return $this->hasMany(Attachment::class);
    }

    function sender(){
        return $this->belongsTo(User::class);
    }

    function recipient(){
        return $this->belongsTo(User::class);
    }
}
