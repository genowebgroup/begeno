<?php
/**
 * Created by PhpStorm.
 * User: zbyszek
 * Date: 9/26/16
 * Time: 11:28 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    function add($translationId, $language, $phrase){

        $this->translationId = $translationId;
        $this->language = $language;
        $this->phrase = $phrase;
        $this->save();
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function value(){
        return $this->phrase;
    }
}